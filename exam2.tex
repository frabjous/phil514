\documentclass[12pt]{article}
\usepackage[margin=1.2in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage[T1]{fontenc}
\usepackage{kpfonts}
\usepackage{enumitem}
\usepackage{mathrsfs}
\usepackage{array}
\usepackage{graphicx}

\setlength{\parindent}{0em}

\renewcommand{\t}[1]{\vdash_{\mathrm{#1}}}
\newcommand{\all}[1]{(\forall #1)}
\newcommand{\allx}{\all{x}}
\newcommand{\ally}{\all{y}}
\newcommand{\allz}{\all{z}}
\newcommand{\some}[1]{(\exists #1)}
\newcommand{\somex}{\some{x}}
\newcommand{\somey}{\some{y}}
\newcommand{\somez}{\some{z}}
\newcommand{\impl}{\rightarrow}
\newcommand{\Iff}{\leftrightarrow}
\newcommand{\schematic}[1]{\mathscr{#1}}
\newcommand{\sA}{\schematic{A}}
\newcommand{\sB}{\schematic{B}}
\newcommand{\On}{\mathrm{On}}

\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
\newcommand{\lschematic}[1]{\scalebox{1.1}{$\mathpzc{#1}$}}
\newcommand{\sF}{\lschematic{f}}
\newcommand{\sT}{\lschematic{t}}
\newcommand{\sG}{\lschematic{g}}
\newcommand{\sH}{\lschematic{h}}
\newcommand{\sR}{\lschematic{r}}
\newcommand{\sK}{\lschematic{c}}
\newcommand{\sU}{\lschematic{u}}
\newcommand{\sV}{\lschematic{v}}
\newcommand{\sW}{\lschematic{w}}
\newcommand{\sX}{\lschematic{x}}
\newcommand{\sY}{\lschematic{y}}
\newcommand{\sZ}{\lschematic{z}}

\begin{document}\pagestyle{empty}
{\bfseries
Mathematical Logic II – Spring 2015\\
Exam 2: Classical Axiomatic Set Theories}

\bigskip

Due by the start of class on Wednesday, April 8th. (Homework due Friday, April 3rd.)

\bigskip

\textbf{THE EXAM HAS TWO PARTS. YOU MUST COMPLETE \underline{FOUR} QUESTIONS IN TOTAL. YOU MUST COMPLETE AT LEAST TWO FROM PART I AND AT LEAST ONE FROM PART II.}~~In other words, you can choose between answering three from part I and one from part II, or answering two from each part.

\bigskip

\textbf{Part I: Object Language Proofs}. For each,  unless otherwise noted, your proof either may be a full, numbered, object-language proof or may be more informal, written in paragraph form. However, all significant parts of the proofs must be included. You may appeal to any theorems or derived rules found on the handouts for the system in question, \emph{unless otherwise noted}. You may also use predicative versions of ZF theorems in NBG (unless it is the theorem you are asked to prove).

\begin{enumerate}[label=(\arabic*), leftmargin=*]

\item Prove: $\t{Z} \allx\ally\allz (x \in y \cap z \Iff x \in y \land x \in z)$. (This is listed on the handouts as T11. Obviously, you are not allowed to use it to prove itself. However, you may use any of T1--T10.) 

\item In ZF, prove \emph{the principle of transfinite induction}, which states that if an ordinal has property P whenever all ordinals less than it have P, then all ordinals have P, i.e., prove:\\
$\t{ZF} \allx(\On(x) \land \ally(y \in x \impl \sA[y]) \impl \sA[x]) \impl \allz(\On(z) \impl \sA[z])$\\
(Hint: the proof is very similar to the proof of T25, the principle of mathematical induction.)

\item Prove: $\t{ZF} \ally\allz\allx(F(y) \land \langle z, x \rangle \in y \impl y\text{``}z = x)$. Use the definitions on the handout, p.\ 46. You may take for granted that (*) $\t{ZF} \ally\allz\allx(\langle z, x \rangle \in y \impl x \in I(y))$.

\item Prove the normal version of the axiom of choice from Hatcher's version. I.e., in ZFC defined as ZF along with the axiom ZFC10. $\allx(x \ne 0 \impl \sigma(x) \in x)$, show\\
$\t{ZFC} \allx \somey (F(y) \land \allz (z \subseteq x \land z \ne 0 \impl y\text{``}z \in z)).$\\
(You may use the theorem proven in question (3) above if desired, whether or not you did question (3).)

\item Prove the multiplicative ``axiom'' in ZFC using the axiom of choice, i.e., prove:\\
$\t{ZFC} \allx(\ally(y \in x \impl y \ne 0) \land \ally\allz(y \in x \land z \in x \land y \ne z \impl y \cap z =0)\ \impl$

\hfill$\somey\allz(z \in x \impl \some{!x_1}(x_1 \in y \cap z))) $
\\
Your proof may be very informal.

\item Prove $\t{ZF} \allx\ally(x\text{-Irr}(y) \land x\text{-Trans}(y) \impl \allz\all{w}(z \in y \land w \in y \land \langle z,w \rangle \in x \impl \langle w, z \rangle \notin x))$. Use the definitions on the handouts, p.\ 50.

\item Taking for granted the following results (with $\On(\sT)$ defined the same for NBG as for ZF):
\begin{enumerate}[nolistsep,label=(\alph*)]
\item $\t{NBG} \allx\ally(\On(x) \land \On(y) \impl x \in y \lor y \in x \lor x =y)$
\item $\t{NBG} \allx\ally(\On(x) \land y \in x \impl \On(y))$, and
\item $\t{NBG} \allx x \notin x$
\end{enumerate}
Prove that $\t{NBG} \text{Pr}(\{z|\On(z)\})$. (Hint: show that otherwise $\{z|\On(z)\} \in \{z|\On(z)\}$.)

\item Taking for granted that:
\begin{enumerate}[nolistsep,label=(\alph*)]
\item $\t{NBG+AC} \allx\ally(\On(x) \land \On(y) \impl x \in y \lor y \in x \lor x = y)$, and
\item $\t{NBG+AC} \allx\somey(\On(y) \land x \cong y)$ 
\end{enumerate}
Prove the trichotomy principle in NBG + AC, viz.:\\$\t{NBG+AC} \allx\ally(x \preceq y \lor y \preceq x)$.

(The definitions of $\preceq$ and $\cong$ are the same as for ZF.) 

\item Prove that $\t{ZFU} \allx \text{K}(\wp(x))$. Make all steps explicit.

\end{enumerate}

\bigskip

\hrulefill

\bigskip

\textbf{Part II: Metatheoretic Results, Philosophical Issues and More.} Your answers should be ``in your own words'', worded and structured in your own way.

\begin{enumerate}[resume, label=(\arabic*), leftmargin=*]

\item
In your own words, sketch the proof that if ZF is consistent, then ST is consistent. (The
proof is scattered in Hatcher on pp.\ 131--34, and pp.\ 165--66.)

\item
Write a 1--2 page essay on the following topic: How does the approach to mathematics
and numbers taken in type-theory differ generally from that taken in contemporary set theories
such as ZF and NBG? Which do you find to be philosophically or mathematically preferable,
and why?

\item
Write a 1--2 page essay on the following topic: What is the difference between cardinal
and ordinal numbers? What are the cardinal and ordinal numbers defined as being in ZF? Do
you find these definitions to be acceptable?

\item
Write a 1--2 page essay on the following topic: What are the primary differences between
the set theories ZF and NBG? Which do you find to be philosophically or mathematically
preferable, and why?

\item
Explain the sketch of the proof using the axiom of choice that every set is equinumerous
with some ordinal (Handouts, pp.\ 49--50) in your own words.

\end{enumerate}



\textit{You are expected to work on your own on the exam (i.e., and not collaborate with your peers); however,
you are encouraged to see me if you need help.}
\end{document}

