\documentclass[12pt]{article}
\usepackage[margin=0.80in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{libertine}
\usepackage[libertine]{newtxmath}
\usepackage{enumitem}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{array}
\usepackage{graphicx}
\renewcommand{\forall}{\forallAlt}
\renewcommand{\exists}{\existsAlt}

\setlength{\parindent}{0em}

\renewcommand{\t}[1]{\vdash_{\mathrm{#1}}}
\newcommand{\all}[1]{(\forall #1)}
\newcommand{\allx}{\all{x}}
\newcommand{\some}[1]{(\exists #1)}
\newcommand{\somex}{\some{x}}
\newcommand{\impl}{\rightarrow}
\newcommand{\schematic}[1]{\mathscr{#1}}
\newcommand{\sA}{\schematic{A}}
\newcommand{\sB}{\schematic{B}}

\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
\newcommand{\lschematic}[1]{\scalebox{1.1}{$\mathpzc{#1}$}}
\newcommand{\sF}{\lschematic{f}}
\newcommand{\sT}{\lschematic{t}}
\newcommand{\sG}{\lschematic{g}}
\newcommand{\sH}{\lschematic{h}}
\newcommand{\sR}{\lschematic{r}}
\newcommand{\sK}{\lschematic{c}}
\newcommand{\sU}{\lschematic{u}}
\newcommand{\sV}{\lschematic{v}}
\newcommand{\sW}{\lschematic{w}}
\newcommand{\sX}{\lschematic{x}}
\newcommand{\sY}{\lschematic{y}}
\newcommand{\sZ}{\lschematic{z}}

\begin{document}\pagestyle{empty}
{\bfseries
Mathematical Logic II – Spring 2015\\
Exam 1: Naïve Foundations, Type-Theory and Higher-Order Logic}

\bigskip

Due by the start of class on Wednesday, March 4th. (Homework due Friday, February 27th.)

\bigskip



\underline{ANSWER ANY FOUR OF THE FOLLOWING QUESTIONS} \qquad (Question \#4 counts as two.)\\
Note, for any question, you may use all standard derived or natural deduction rules for classical logic, along with whatever derivation notation(s) and abbreviations you may prefer, provided that you let me know what any unusual/uncommon abbreviation or notation means.

\begin{enumerate}[label=(\arabic*),leftmargin=*]
\item Without making use of Russell's paradox or other contradiction, prove the following for Hatcher's System F.\\
T15 $\t{F} \Lambda \in \omega$\\
T16 $\t{F} \allx(\Lambda \ne x \cup \{x\})$\\
T17 $\t{F} \allx(x \in \omega \impl x \cup \{x\} \in \omega)$\\
T18 $\t{F} \allx(\Lambda \in x \land \all{z}(z \in x \impl z \cup \{z\} \in x) \impl \omega \subseteq x)$\\
T19 $\t{F} \sA[\Lambda] \land \allx(\sA[x] \impl \sA[x \cup \{ x \}]) \impl \allx(x \in \omega \impl \sA[x]) $\\
You may use any theorems proved prior to these theorems (T1--T14). See hints in both the handout and in Hatcher, p.\ 91. In a short paragraph, explain what purpose these theorems serve in obtaining the desired principles of natural number theory in F.

\item Prove the following for the simple theory of types for classes (System ST):\\
$\t{ST} \all{x_1^n}\all{x_2^n}\all{y_1^n}\all{y_2^n}(\langle x_1^n, y_1^n \rangle = \langle x_2^n, y_2^n \rangle \impl x_1^n = x_2^n \land y_1^n = y_2^n)$\\
You may use any of the theorems T1--T13 from the handouts; recall that $\langle  \sT, \sU \rangle$ is defined as $\{ \{\sT\}, \{\sT, \sU \} \}$.

\item A theorem of the form:\\
(\%) $\t{F} \all{x_1}\all{x_2}\all{y_1}\all{y_2}(\langle x_1, y_1 \rangle = \langle x_2, y_2 \rangle \leftrightarrow x_1 = x_2 \land y_1 = y_2)$\\
Can be proved for system F (without using a contradiction) just as for system ST. You may take this result for granted (whether or not you did question 2). \\
\hspace*{2em} Now, consider the set $R$, where $R$ is defined as $\{z | \some{x}\some{y}(z = \langle x, y \rangle \land z \notin x)\}$.\\
\hspace*{2em} Using $R$, find a \emph{different} way of proving that system $F$ is inconsistent (i.e., without making use of Russell's paradox, or Cantor's paradox from (5) below, prove a contradiction in system F involving $R$).

\item \emph{(Counts as two questions!)} Consider the following definitions for HOPC + (Ext):\\
$\sF^{(\tau)} \cong \sG^{(\tau)}$~~for~~$\some{\sR^{(\tau, \tau)}}(
    \all{\sX^\tau}\all{\sY^\tau}\all{\sZ^\tau}(\sR(\sX,\sY) \land \sR(\sX,\sZ) \impl \sY=\sZ)\,\land$\\
    \phantom{.}\hspace*{7em} $\all{\sX}\all{\sY}\all{\sZ}(\sR(\sY,\sX) \land \sR(\sZ,\sX) \impl \sY=\sZ)\,\land$\\
    \phantom{.}\hfill $\all{\sX}(\sF(\sX) \impl \some{\sY}(\sG(\sY) \land \sR(\sX, \sY))) \land
\all{\sY}(\sG(\sY) \impl \some{\sX}(\sF(\sX) \land \sR(\sX, \sY)))
)$\\
$\#(\sF^{(\tau)})$~~for~~$[\lambda \sG^{(\tau)}~\sF \cong \sG]$\\
Prove the following:\\
\begin{tabular}{ >{\begin{math}}l<{\end{math}}l}
\vdash \all{F^{(\tau)}}F \cong F &(Reflexivity of equinumerosity)\\
\vdash \all{F^{(\tau)}}\all{G^{(\tau)}}(F \cong G \impl G \cong F)&
(Symmetry of equinumerosity)\\
\vdash \all{F^{(\tau)}}\all{G^{(\tau)}}\all{H^{(\tau)}}(
F \cong G \land G \cong H \impl F \cong H
)&(Transitivity of equinumerosity)\\
\vdash \all{F^{(\tau)}}\all{G^{(\tau)}}(\#(F) = \#(G) \leftrightarrow F \cong G)
&(Hume's law; requires (Ext))
\end{tabular}

\bigskip

(over)

\clearpage
\item \emph{Cantor's Paradox for F:} Consider the following definitions for system F:\\
$\sT \cong \sU$~~for~~$\some{\sW}( \all{\sX} \all{\sY} \all{\sZ}(\langle \sX, \sY \rangle \in \sW \land \langle \sX, \sZ \rangle \in \sW \impl \sY = \sZ)\, \land $\\
\hspace*{3em}$\all{\sX}\all{\sY}\all{\sZ}(\langle \sY, \sX \rangle \in \sW \land \langle \sZ, \sX \rangle \in \sW \impl \sY = \sZ)\,\land$\\
\phantom{.}\hfill$\all{\sX}(\sX \in \sT \impl \some{\sY}(\sY \in \sU \land \langle \sX, \sY \rangle \in \sW)) \land \all{\sY}(\sY \in \sU \impl \some{\sX}(\sX \in \sT \land \langle \sX, \sY \rangle \in \sW)))$\\
$\wp(\sT)$~~for~~$\{\sX | \sX \subseteq \sT\}$\\
Whether or not you did question (4), you may take the following for granted (the proofs are similar to those in question (4)):\\
\begin{tabular}{l >{\begin{math}}l<{\end{math}}}
(Ref$\cong$) & \t{F}\allx x \cong x\\
(Sym$\cong$) & \t{F}\allx \all{y} (x \cong y \impl y \cong x)\\
(Trans$\cong$) & \t{F}\allx \all{y} \all{z} (x \cong y \land y \cong z \impl x \cong z)
\end{tabular}\\
Without making use of Russell's paradox, or the contradiction from problem (3), or anything similar, prove system F inconsistent by proving the following two theorems:\\
\hspace*{1.5em} (a) $\t{F} \allx \neg (x \cong \wp(x))$\\
\hspace*{1.5em} (b) $\t{F} \mathrm{V} \cong \wp(\mathrm{V})$\\
You may use any theorem from the handouts or book for System F (except those for Russell's paradox).\\
\emph{Hint for (a):} assume for reductio that $x \cong \wp(x)$. Existentially instantiate the $\some{\sW}$ to ``$r$''. Consider $W = \{z | z \in x \land \all{y}(\langle z, y \rangle \in r \impl z \notin y)\}$. Show $W \in \wp(x)$ and hence that there is some $a$ such that $a \in x$ and $\langle a, W \rangle \in r$. Prove that $a \in W \leftrightarrow a \notin W$ and complete the reductio. \\
\emph{Hint for (b):} use T27 to prove that $\mathrm{V} = \wp(\mathrm{V})$.

\item In system GG, without making use of Russell's paradox or similar contradiction, prove what is sometimes called \emph{Frege's Double Correlation Thesis}:\\
(DCT) \qquad $\t{GG}\some{F}\all{G}(M_\beta (G(\beta)) \leftrightarrow F(\{x|G(x)\}))$\\
You may also assume: $\t{GG} \all{F}\all{G}(\allx(F(x) = G(x)) \impl (M_\beta(F(\beta)) \leftrightarrow M_\beta (G(\beta))))$.\\ (Note that if we had third-order quantifiers, we could generalize on these to get $\all{M}\ldots$, etc.)

\item  Write a 1--2 page essay explaining informally how it is that the paradoxes such as Russell’s
paradox, and those from questions (3) and (5) are avoided in the simple theory of types (ST).

\item Write a 1--2 page essay exploring the philosophical issues surrounding type theory. (You
may focus on either type theory for sets or classes, or the higher-order versions.) You may
address any or all of the following questions: Is type theory philosophically plausible? Are
ramified or predicative type theories philosophically preferable to impredicative or simple
type theories? Does type theory make for a plausible foundations for arithmetic?

\item In your own words, sketch—(only sketch, do not show every step in detail)—the proof that
the pure predicate calculus of second order is standardly incomplete.

\item In your own words, explain the difference between standard semantics for higher-order theories and general (or Henkin) semantics. Explain how it is that second-order logic (or other variety) can be incomplete with respect to standard models but complete with regard to general models.
\end{enumerate}
\emph{You are expected to work on your own on the exam (i.e., and not collaborate with your peers); however, you may see me if you need help.}

\end{document}

